//
//  ProfileApiRouter.swift
//  Backit
//
//  Created by Александр Кузьмин on 27/10/2019.
//  Copyright © 2019 Ivan Nikitin. All rights reserved.
//

import Foundation
import Alamofire

enum ProfileApiRouter: BaseApiRouter {
    
    case profile(clientId: String)
    case subscription(type: String, status: Int)

    // MARK: - HTTPMethod
    internal var method: HTTPMethod {
        switch self {
        case .profile:
            return .get
        case .subscription:
            return .put
        }
    }
    
    // MARK: - Path
    internal var path: String {
        switch self {
        case .profile:
            return "/user/profile"
        case .subscription:
            return "/user/profile/subscriptions"
        }
    }
    
    // MARK: - Parameters
    internal var parameters: Parameters? {
        switch self {
        case .profile(let clientId):
            return [Constants.APIParameterKey.clientId : clientId]
        case .subscription(let type,let status):
            return [Constants.APIParameterKey.type : type, Constants.APIParameterKey.status : status]
        }
    }
    
    // MARK: - Timeout
    internal var timeout: TimeInterval {
        switch self {
        default:
            return 10
        }
    }
    
    // MARK: - QueryType
    internal var queryType: Query {
        switch self {
        case .subscription:
            return .json
        default:
            return .path
        }
    }
    
    // MARK: - Headers
    var headers: HTTPHeaders {
        switch self {
        default:
            return defaultHeader()
        }
    }
    
    // MARK: - BaseUrl
    var baseUrl: URL? {
        return nil
    }
    
}
