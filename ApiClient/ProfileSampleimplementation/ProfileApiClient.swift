//
//  ProfileApiClient.swift
//  Backit
//
//  Created by Александр Кузьмин on 27/10/2019.
//  Copyright © 2019 Ivan Nikitin. All rights reserved.
//

import Foundation
import Alamofire

class ProfileApiClient: BaseApiClient {

    static func profile(completion:@escaping (Result<ProfileResponse, Error>)->Void) {
        performRequest(router: ProfileApiRouter.profile(clientId: Session.shared.client_id), completion: completion)
    }
    
    static func subscription(type: String, status: Int, completion:@escaping (Result<SubscriptionResponse, Error>)->Void) {
        performRequest(router: ProfileApiRouter.subscription(type: type, status: status), completion: completion)
    }
    
}
