//
//  AccountMenuHeaderReusableViewMode.swift
//  CashBackEPN
//
//  Created by Ivan Nikitin on 28/01/2019.
//  Copyright © 2019 Ivan Nikitin. All rights reserved.
//

import UIKit
import XCoordinator

class AccountViewModel: NSObject {
    
    private let router: UnownedRouter<AccountRoute>
    
    init(router: UnownedRouter<AccountRoute>) {
        self.router = router
    }
    
    func loadUserData(completion: (()->())?, failure: (()->())?) {
        //EXAMPLE OF USE ApiClient
        ProfileApiClient.profile { [weak self] (result) in
            switch result {
            case .success(let response):
                self?.router.trigger(.detailProfile(response))
                break
            case .failure(let error):
                Alert.showErrorAlert(by: error)
                failure?()
                break
            }
        }
    }
    
}
